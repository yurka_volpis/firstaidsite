(function ($) {

Drupal.behaviors.jaOrder = {
  attach: function(context, settings) {
    $('.chatlogs').scrollTop($('.chatlogs')[0].scrollHeight);


    var user_id = settings.user_id;
    var doctor_id = settings.doctor_id;
    var time = settings.time;
    console.log(user_id);
    console.log(doctor_id);
    console.log(time);
    console.log('/api/v1/need_reload_page' + '?doctor_uid=' + doctor_id + '&user_id=' + user_id + '&time=' + time);


    window.setInterval(function(){

      $.ajax({
          url: '/api/v1/need_reload_page' + '?doctor_uid=' + doctor_id + '&user_id=' + user_id + '&time=' + time,
          type: 'GET',
          dataType: 'json',
          success: function (results) {
            console.log(results);
            if (results.need_reload) {
              location.reload();
            }
          },
          complete: function () {
          }
        });
    }, 3000);


  }
}
})(jQuery);
